@extends('web.layout.main')

@section('content')


<div class="login-box"> 

    <div class="login-box-body">

        @if($success==true)
        <div class="alert alert-success">
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            <div class="msg"> Your account has been successfully verified! You can now use the app using your credentials!</div>
        </div>
        @else   
        <div class="alert alert-danger">
            <h4><i class="icon fa fa-ban"></i> Error!</h4>
            <div class="msg">This link is either invalid or expired, please contact admin!</div>
        </div>
        @endif  

    </div>
    <!-- /.login-box-body -->

</div>



</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="{{asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>
@stop