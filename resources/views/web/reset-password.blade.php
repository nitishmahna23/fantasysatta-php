@extends('web.layout.main')

@section('content')


 <div class="login-box"> 

  <div class="login-box-body">

     @if(!$success)
        <div class="alert alert-danger">
           <h4><i class="icon fa fa-ban"></i> Error!</h4>
           <div class="msg">{{$message}}</div>
        </div>
     @else   
            <p class="login-box-msg">Reset Password</p>
           
            <form action="{{url('api/v1/user/reset-password')}}" method="post">
               <input class="form-control" type="hidden"  name="userId" value="{{$token_details['user_id']}}" placeholder="" required>
              <div class="form-group has-feedback">
                <input type="password" id="password" name="password" minlength=8 maxlength=25 class="form-control" placeholder="Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" id="confirm_password" name="password_confirmation" minlength=8 maxlength=25 class="form-control" placeholder="Confirm Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="row">
                <!-- /.col -->
                <div class="col-xs-4">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
                </div>
                <!-- /.col -->
              </div>
            </form>
  @endif  

  </div>
  <!-- /.login-box-body -->

</div>



</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="{{asset('AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/reset-password.js')}}" ></script>
</body>
</html>
@stop