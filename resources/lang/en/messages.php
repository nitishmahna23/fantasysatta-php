<?php

return [
    'org_name'=>'Oil Price',
    'auth_required' => 'Authenitication required',
    'bad_request' => 'Invalid JSON.',
    'register_success' => 'Thank you for joining FantasySatta. To finish signing up, please verify your email.',
    'exception_msg' => 'Something went wrong please contact Admin!',
    'fail_login' => 'Failed to login! Please try again!',
    'account_inactive' => 'Your account is not active, Please contact admin!',
    'login_success' => 'Login successful!',
    'logout' => 'Logged out successfully!',
    'invalid_login_credentials' => 'Either the email or the password is invalid!',
    'password_changed' => 'Password changed successfully!',
    'forgot_pwd_email' => 'An email with the reset password link has been sent to your email id!',
    'invalid_old_password' => 'Your old password is incorrect',
    'verification_token_expired' => 'The verification token has expired!',
    'cities_updated' => 'Cities has been updated successfully!',
    'price_updated' => 'Oil Price has been updated successfully!',
    'price_table_truncated' => 'Old data has been deleted successfully!',
    'org_name' => 'FantasySatta',
    'email_verification_subject' => 'FantasySatta : Email Verification'
];
