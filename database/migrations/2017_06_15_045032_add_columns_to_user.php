<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('user', function (Blueprint $table) {
            $table->unsignedInteger('follower_count')->default(0)->after('status');
            $table->unsignedInteger('subscriber_count')->default(0)->after('follower_count');
            $table->decimal('balance', 10, 2)->nullable()->default('0.00')->after('funding_account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('user', function (Blueprint $table) {
            //
        });
    } 

}
