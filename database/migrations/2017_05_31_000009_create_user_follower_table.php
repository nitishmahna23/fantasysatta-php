<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserFollowerTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'user_follower';

    /**
     * Run the migrations.
     * @table user_follower
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->comment('primary key of table');
            $table->unsignedInteger('follower_id')->nullable()->default(null);
            $table->unsignedInteger('followee_id')->nullable()->default(null);

            $table->index(["follower_id"], 'follower_id');

            $table->index(["followee_id"], 'followee_id');

            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('follower_id', 'idx_follower_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('followee_id', 'idx_followee_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
