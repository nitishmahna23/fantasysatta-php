<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserStockTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'user_stock';

    /**
     * Run the migrations.
     * @table user_stock
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->unsignedInteger('stock_symbol_id')->nullable()->default(null);
            $table->unsignedInteger('Qty');
            $table->decimal('price', 10, 2);

            $table->index(["stock_symbol_id"], 'stock_symbol_id');

            $table->index(["user_id"], 'user_id');

            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('user_id', 'idx_user_stock_user_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('stock_symbol_id', 'idx_user_stock_stock_symbol_id')
                    ->references('id')->on('stock_symbol')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
