<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateGroupMemberTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'group_member';

    /**
     * Run the migrations.
     * @table group_member
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('group_id')->nullable()->default(null);
            $table->unsignedInteger('user_id')->nullable()->default(null);

            $table->index(["group_id"], 'group_id');

            $table->index(["user_id"], 'user_id');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('group_id', 'idx_group_member_group_id')
                    ->references('id')->on('user_group')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('user_id', 'idx_group_member_user_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
