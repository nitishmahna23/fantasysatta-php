<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'user';

    /**
     * Run the migrations.
     * @table user
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->comment('primary key of table');
            $table->string('email', 55)->nullable()->default(null);
            $table->string('password', 100)->nullable()->default(null);
            $table->string('username', 50)->nullable()->default(null);
            $table->string('name', 100)->nullable()->default(null);
            $table->string('profile_image')->nullable()->default(null);
            $table->string('facebook_id')->nullable()->default(null);
            $table->string('phone', 15)->nullable()->default(null);
            $table->string('tpt_account_id')->nullable()->default(null);
            $table->string('funding_account_id')->nullable()->default(null);
            $table->unsignedInteger('referred_user_id')->nullable()->default(null)->index();
            $table->unsignedTinyInteger('tpt_account_status')->nullable()->default(null);
            $table->unsignedTinyInteger('funding_account_status')->nullable()->default(null);
            $table->unsignedTinyInteger('role_type')->default('2')->comment('1=>Admin,2=>User');
            $table->unsignedTinyInteger('status')->default('0')->comment('0=>Not Approved,1=>Approved')->index();
            $table->string('referral_code')->nullable()->default(null);
            $table->unsignedTinyInteger('is_public')->default('1')->comment('1=>public,2=>private');
            $table->unsignedTinyInteger('is_email_verified')->default('0');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
