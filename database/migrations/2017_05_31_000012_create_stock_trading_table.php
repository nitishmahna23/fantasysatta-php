<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateStockTradingTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'stock_trading';

    /**
     * Run the migrations.
     * @table tpt_funding_transaction
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->comment('primary key of table');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('stock_symbol_id')->nullable()->index();
            $table->unsignedInteger('Quantity')->nullable();
            $table->unsignedTinyInteger('type')->comment('1=>In,2=>Out');
            $table->unsignedTinyInteger('order_type')->comment('1=>market,2=>limit,3=>stop,4=>stop_limit');
            $table->decimal('limit_price', 10, 2)->nullable()->default('0.00');
            $table->decimal('stop_price', 10, 2)->nullable()->default('0.00');
            $table->unsignedTinyInteger('allocation_type')->nullable()->comment('1=>shares,2=>cost_limit');
            $table->text('tpt_legs_json')->nullable();
            $table->text('tpt_allocations_json')->nullable();
            $table->unsignedTinyInteger('status')->comment('0=>Pending,1=>Completed');
            $table->dateTimeTz('tpt_timestamp');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('user_id', 'idx_stock_trading_user_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            $table->foreign('stock_symbol_id', 'idx_stock_trading_stock_symbol_id')
                    ->references('id')->on('stock_symbol')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
