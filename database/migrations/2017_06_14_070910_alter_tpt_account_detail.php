<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTptAccountDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tpt_account_detail', function (Blueprint $table) {
           $table->string('employement_status',20)->nullable()->default(null)->change();
           $table->string('applicant_id')->nullable()->default(null)->after('user_id');
           $table->date('birthday')->after('state');
           $table->renameColumn('employement_status', 'employment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tpt_account_detail', function (Blueprint $table) {
            //
        });
    }
}
