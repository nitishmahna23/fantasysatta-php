<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTptFundingTransactionTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tpt_funding_transaction';

    /**
     * Run the migrations.
     * @table tpt_funding_transaction
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->comment('primary key of table');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->string('transfer_id', 255)->nullable();
            $table->string('funding_acc_id')->nullable();
            $table->decimal('amount', 10, 2)->nullable()->default('0.00');
            $table->unsignedTinyInteger('type')->comment('1=>In,2=>Out');
            $table->unsignedTinyInteger('status')->comment('0=>Pending,1=>Completed');
            $table->decimal('fees', 10, 2)->nullable()->default('0.00');
            $table->string('notes',1)->nullable();
            $table->dateTimeTz('tpt_timestamp');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('user_id', 'idx_tpt_funding_transaction_user_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
