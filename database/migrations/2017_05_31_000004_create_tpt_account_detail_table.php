<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTptAccountDetailTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tpt_account_detail';

    /**
     * Run the migrations.
     * @table tpt_account_detail
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->comment('primary key of table');
            $table->unsignedInteger('user_id')->nullable()->default(null)->comment('primary key of table');
            $table->string('tpt_account_id')->nullable()->default(null);
            $table->string('first_name', 100)->nullable()->default(null);
            $table->string('last_name', 100)->nullable()->default(null);
            $table->string('country', 50)->nullable()->default(null);
            $table->string('citizenship', 20)->nullable()->default(null);
            $table->string('ssn', 15)->nullable()->default(null);
            $table->unsignedTinyInteger('employement_status')->nullable()->default(null);
            $table->string('address1', 100)->nullable()->default(null);
            $table->string('address2', 100)->nullable()->default(null);
            $table->string('zip', 20)->nullable()->default(null);
            $table->string('city', 50)->nullable()->default(null);
            $table->string('state', 50)->nullable()->default(null);

            $table->index(["user_id"], 'user_id');
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('user_id', 'idx_tpt_account_detail_user_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
