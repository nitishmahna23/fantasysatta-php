<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserFeedTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'user_feed';

    /**
     * Run the migrations.
     * @table user_feed
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->unsignedTinyInteger('type')->nullable()->default(null)->comment('type of notification includes business, event, buds');
            $table->unsignedInteger('resource_id')->nullable()->default(null)->comment('reference in  the respective type');
            $table->string('message')->nullable()->default(null);

            $table->index(["user_id"], 'user_id');

            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('user_id', 'idx_feed_user_id')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('user_id', 'idx_feed_user_id_1')
                    ->references('id')->on('user')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->set_schema_table);
    }

}
