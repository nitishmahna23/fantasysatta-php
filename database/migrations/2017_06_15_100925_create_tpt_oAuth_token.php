<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTptOAuthToken extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tpt_oauth_token';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('access_token')->nullable();
            $table->datetime('expiry')->nullable()->default(null);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tpt_oAuth_token', function (Blueprint $table) {
            //
        });
    }

}
