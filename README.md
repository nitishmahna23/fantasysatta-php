# Alfa 

## Introduction
This is a trading app 

## Requirements

Package                                                | Version
------------------------------------------------------------- | ----------------
PHP | 7
Laravel | 5.3
Yajra | 6.0

## Servers

Domain                                                | Url
------------------------------------------------------------- | ----------------
Dev | https://dev.alfa-investor.com
QA | https://qa.alfa-investor.com
Staging | https://staging.alfa-investor.com
Live | https://www.alfa-investor.com

### Authors
- Atul Gupta (atul.gupta@appster.in)

### Dependency

- [Yajra]

### System Requirement
- [Laravel](https://laravel.com/)
### SkillSet
- PHP
- Laravel framework

### License

© 2016-17 Appster, All rights reserved.