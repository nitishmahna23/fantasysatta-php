<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get('/user', function (Request $request) {
    return $request->user();
}); //->middleware('auth:api');


Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'user'], function() {
        Route::post('register', 'Api\v1\UserController@register');
        Route::post('login', 'Api\v1\UserController@login');
        Route::post('fb-register', 'Api\v1\UserController@fbRegister');
        Route::post('update-profile', 'Api\v1\UserController@updateProfile');
        Route::get('logout', 'Api\v1\UserController@logout');
        Route::post('change-password', 'Api\v1\UserController@changePassword');
        Route::post('forgot-password', 'Api\v1\UserController@forgotPassword');
        Route::post('reset-password', 'Api\v1\UserController@resetPassword');
        Route::post('update-image', 'Api\v1\UserController@updateProfileImage');
        Route::get('profile-detail', 'Api\v1\UserController@getProfileDetail');
    });

    Route::group(['prefix' => 'crawler'], function() {
        Route::post('scrape-city', 'Api\v1\WebScraperController@scrapeCity');
        Route::post('scrape-price/{oilType}', 'Api\v1\WebScraperController@scrapeOilPrice');
        Route::post('delete-old-price', 'Api\v1\WebScraperController@truncateOilPrice');
    });


    Route::get('/terms', function () {
        return view('pages.terms_conditions');
    });

    Route::get('/privacy', function () {
        return view('pages.privacy_policy');
    });
    
    Route::post('fetch-oil-price', 'Api\v1\OilPriceController@fetchOilPrice');
    
    Route::get('fetch-cities', 'Api\v1\OilPriceController@fetchCities');
});



