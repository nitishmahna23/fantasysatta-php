swagger: '2.0'
info:
  version: 1.0.0
  title: Petrol Pricing App
  description: |
    #### Echos back every URL, method, parameter and header
    Feel free to make a path or an operation and use **Try Operation** <?php echo $_SERVER['REQUEST_SCHEME']; ?>://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo $base_path = strstr($_SERVER['REQUEST_URI'],'/public') ? explode('/public', dirname($_SERVER['REQUEST_URI']))[0].'/public/api/v1' : '/api/v1'; ?> to test it. The echo server will
    render back everything.
schemes:
  - "<?php echo $_SERVER['REQUEST_SCHEME']; ?>"
host: <?php echo $_SERVER['SERVER_NAME']; ?>

basePath: "<?php echo $base_path = strstr($_SERVER['REQUEST_URI'],'/public') ? explode('/public', dirname($_SERVER['REQUEST_URI']))[0].'/public/api/v1' : '/api/v1'; ?>"
paths:   
  /crawler/scrape-city:
    post:
      description: Fetch cities API
      responses:
        200:
          description: Success response
          schema:
             $ref: '#/definitions/User'
        default:
          description: Unexpected error
          schema:
             $ref: '#/definitions/Error'
  /crawler/scrape-price/{oilType}:
    post:
      description: Fetch prices API
      responses:
        200:
          description: Success response
          schema:
             $ref: '#/definitions/User'
        default:
          description: Unexpected error
          schema:
             $ref: '#/definitions/Error'
      parameters:
        - name: oilType
          required: true
          in: formData
          type: integer
  /crawler/delete-old-price:
    post:
      description: Delete old price API
      responses:
        200:
          description: Success response
          schema:
             $ref: '#/definitions/User'
        default:
          description: Unexpected error
          schema:
             $ref: '#/definitions/Error'
  /fetch-oil-price:
    post:
      description: Fetch oil prices API
      responses:
        200:
          description: Success response
          schema:
             $ref: '#/definitions/User'
        default:
          description: Unexpected error
          schema:
             $ref: '#/definitions/Error'
      parameters:
        - name: city_id
          required: true
          in: formData
          type: integer
        - name: oil_type
          required: true
          in: formData
          type: integer
  /fetch-cities:
    get:
      description: Delete old price API
      responses:
        200:
          description: Success response
          schema:
             $ref: '#/definitions/User'
        default:
          description: Unexpected error
          schema:
             $ref: '#/definitions/Error'
  /terms:
    get: 
      description: Terms & Conditions Page
      responses:
        default:
          description: Terms & Conditions Page
  /privacy:
    get: 
      description: Privacy Policy Page
      responses:
        default:
          description: Privacy Policy Page
definitions: 
   Error:
    type: object
    properties:
      status:
        type: boolean
      message:
        type: string
      errors:
        type: object
        description: Key/value errors for each field
   User:
          type: object
          properties:
            id: 
               type: integer
            email: 
               type: string
            username: 
               type: string
            name: 
               type: string
            profileImage: 
               type: string
            facebookId: 
               type: string
            phone: 
               type: string
            tptAccountId: 
               type: string
            fundingAccountId: 
               type: string
            tptAccountStatus: 
               type: integer
            fundingAccountStatus: 
               type: integer
            roleType: 
               type: integer
            status: 
               type: integer
               description: STATUS_UNAPPROVED = 0;STATUS_APPROVED = 1;STATUS_INCOMPLETE_PROFILE = 2;
            followerCount: 
               type: integer
            subscriberCount: 
               type: integer
            referralCode: 
               type: string
            isPublic: 
               type: integer
            isEmailVerified: 
               type: integer
   UserProfile:
    type: object
    properties:
      user:
        type: object
        schema:
             $ref: '#/definitions/User'
      transactionHistory:
        type: object
