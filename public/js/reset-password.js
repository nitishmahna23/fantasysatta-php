$('form').on('submit', function (e) {
    e.preventDefault();

    if ($('#password').val() != $('#confirm_password').val())
    {
        setErrorMessage("The password confirmation doesn't match");
        return false;
    }
    else
    {
        $('#error').remove();
    }

    var url = $(this).attr('action');
    var postData = $(this).serialize();
    $.ajax({
        method: "post",
        url: url,
        data: postData,
        success: function (data) {
            if (data.success)
            {
                setSuccessMessage(data.message);
                $('form').remove();
                $('.login-box-msg').remove();

            }
            else
            {
                setErrorMessage(data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var error_response = jqXHR.responseJSON;
            setErrorMessage(error_response.message);
        }
    });
});

function setErrorMessage(msg)
{
    var html = '<div id="error" class="alert alert-danger">' +
            '<h4><i class="icon fa fa-ban"></i> Error!</h4>' +
            '<div class="msg">' + msg + '</div>';

    $('#error').remove();
    $('.login-box-body').prepend(html);
}

function setSuccessMessage(msg)
{
    var html = '<div id="success" class="alert alert-success">' +
            '<h4><i class="icon fa fa-check"></i> Success !</h4>' +
            '<div class="msg">' + msg + '</div>';

    $('#error').remove();
    $('.login-box-body').prepend(html);
}