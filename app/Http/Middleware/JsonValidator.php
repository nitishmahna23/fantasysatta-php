<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Controller;

/**
 * JsonValidator Middleware
 *
 * This middleware is used as to validate json input of all incoming request.
 *
 * @author Jitender <jitender.thakur@appster.in>
 */
class JsonValidator {
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $data = $request->getContent();
        if (empty($data)) {
            $obj = new Controller();
            return $obj->respondBadRequest(trans('messages.error.bad_request'));
        }
        return $next($request);
    }

}