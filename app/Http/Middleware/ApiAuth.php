<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\BaseController;
use Symfony\Component\HttpFoundation\Response;
use App\Model\User;

class ApiAuth {

    protected $app_user_tokens = 'app_user_token';
    protected $user_table = 'user';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $user = DB::table($this->user_table)
                ->whereIn($this->user_table . '.status', array(User::STATUS_APPROVED,  User::STATUS_INCOMPLETE_PROFILE))
                ->join($this->app_user_tokens, $this->user_table . '.id', '=', $this->app_user_tokens . '.user_id')
                ->where($this->app_user_tokens . '.access_token', '=', $request->header('accessToken'))
                ->select($this->user_table.'.id')
                ->first();


        if (!is_object($user)) {
            $response['message'] = trans('messages.auth_required');
            $response['success'] = false;
            $response['status_code'] = Response::HTTP_UNAUTHORIZED;
            $base_controller = new BaseController();
            return $base_controller->sendJsonResponse($response);
        }
        Auth::loginUsingId($user->id);
        return $next($request);
    }

}
