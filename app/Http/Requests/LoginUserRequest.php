<?php

namespace App\Http\Requests;

use App\Model\AppUserToken;
use App\Model\User;

class LoginUserRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => 'required|email',
            'password'=>'required',
            'roleType' => 'required|in:'.User::ROLE_TYPE_APP_USER,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
