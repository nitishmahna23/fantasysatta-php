<?php

namespace App\Http\Requests;

use App\Model\User;
use App\Model\AppUserToken;
use App\Http\Requests\BaseRequest;

class EmailSignupRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [

            'email' => 'required|email|unique:users,email,NULL,user_id,deleted_at,NULL,role_type,' . User::ROLE_TYPE_APP_USER,
            'password' => 'required|min:8|max:25',
            'username' => 'required|unique:users',
            'name' => 'required',
            'mobileNumber' => 'required|numeric',
            'roleType' => 'required|in:' . User::ROLE_TYPE_APP_USER,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
