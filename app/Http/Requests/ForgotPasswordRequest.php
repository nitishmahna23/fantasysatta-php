<?php

namespace App\Http\Requests;

use App\Model\User;

class ForgotPasswordRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => 'required|email|exists:user,email',
            'roleType' => 'required|in:' . User::ROLE_TYPE_APP_USER
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'email.exists' => 'This email is not registered with us!'
        ];
    }

}
