<?php

namespace App\Http\Requests;

use App\Model\User;
use App\Model\AppUserToken;

class FbSignupRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'facebookId' => 'required',
            'fbAccessToken' => 'required',
            'roleType' => 'required|in:' . User::ROLE_TYPE_APP_USER,
            'deviceId' => 'required',
            'deviceType' => 'required|in:' . AppUserToken::DEVICE_TYPE_IOS,
            'deviceToken' => 'sometimes',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
