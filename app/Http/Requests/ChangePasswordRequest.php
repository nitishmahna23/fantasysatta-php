<?php

namespace App\Http\Requests;

class ChangePasswordRequest extends BaseRequest{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'oldPassword' => 'required',
            'newPassword' => 'required|min:8'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
