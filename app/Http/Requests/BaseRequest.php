<?php

namespace App\Http\Requests;

use Symfony\Component\HttpFoundation\Response;

/**
 * This class is base class for all API request
 * @author Shyam
 */
class BaseRequest extends Request {

    protected $response = null;

    /**
     * Get data to be validated from the request.
     * This method is used to get json input for APIs and validate the data
     * @return array
     */
    protected function validationData() {
        $postData = Request::all();
        return $postData;
    }

    /**
     * This method is used to send custom response when validation fails
     * @param array $errors
     * @return type
     */
    public function response(array $errors) {
        if (Request::isMethod('post') && empty(Request::all())) {
            $this->response['message'] = trans('messages.bad_request');
        } else {
            $first_error = '';
            foreach ($errors as $error) {
                $first_error = $error[0];
                break;
            }
            $this->response['message'] = $first_error;
            $this->response['errors'] = $errors;
        }
        $this->response['success'] = false;
        return \Illuminate\Support\Facades\Response::json($this->response, Response::HTTP_BAD_REQUEST)->header('Content-Type', "application/json");
    }

    /**
     * This method is used to prevent the inputs from XSS.
     * @param  string  $key
     * @param  string|array|null  $default
     * @return string|array
     */
    public function input($key = null, $default = null) {
        $input = parent::input();

        array_walk_recursive($input, function(&$val) {
            $val = htmlspecialchars($val);
        });
        return data_get($input, $key, $default);
    }

}
