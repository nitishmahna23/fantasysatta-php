<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Request;
use App\Providers\UserServiceProvider;

/**
 * Render static HTML pages 
 * 
 * This class is contains html pages for Terms & Conditions, Privacy Policy etc 
 * 
 */
class PagesController extends BaseController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
     
    }

    
    /**
     * Used to render email verification page
     * @return email verification page
     */
   
    public function verifyEmail() {
        $response = UserServiceProvider::verifyEmail(Request::all());
        return view('emails.verify_email_result', $response);
    }
    
    public function resetPassword(\Illuminate\Http\Request $request) {
        $response = UserServiceProvider::getVerificationTokenDetails($request->all());
        return view('web.reset-password', $response);
    }

}
