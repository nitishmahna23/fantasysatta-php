<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\BaseController;
use App\Http\Requests\EmailSignupRequest;
use App\Http\Requests\LoginUserRequest;
use App\Providers\UserServiceProvider;
use App\Http\Requests\FbSignupRequest;
use App\Providers\FbServiceProvider;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UpdateProfileImageRequest;
use App\Http\Requests\UserProfileRequest;
use Illuminate\Http\Request;

/**
 * This controller is used to handle operations related to user
 * @author Shyam<shyam.pandey@appster.in>
 */
class UserController extends BaseController {

    /**
     * Create a new controller instance.
     * Check for authentication
     * @return void
     */
    public function __construct() {
//        $this->middleware('apiauth', ['except' => ['register', 'login', 'fbRegister', 'forgotPassword', 'resetPassword']]);
    }

    /**
     * Register user using email and password
     * @param EmailSignupRequest $request
     * @return type
     */
    public function register(EmailSignupRequest $request) {
        $resonse = UserServiceProvider::registerUser($request->all());
        return $this->sendJsonResponse($resonse);
    }

    /**
     * Logs in user
     * @param LoginUserRequest $request
     * @return type
     */
    public function login(LoginUserRequest $request) {
        $resonse = UserServiceProvider::loginUser($request->all());
        return $this->sendJsonResponse($resonse);
    }

    /**
     * Logout user
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function logout(\Illuminate\Http\Request $request) {
        $resonse = UserServiceProvider::logout($request->header('accessToken'));
        return $this->sendJsonResponse($resonse);
    }

    /**
     * Update the user details
     * @param UpdateProfileRequest $request
     * @return type
     */
    public function updateProfile(UpdateProfileRequest $request) {
        $resonse = UserServiceProvider::updateProfile($request->all());
        return $this->sendJsonResponse($resonse);
    }

    /**
     * Connect with facebook
     * @param FbSignupRequest $request
     * @return type
     */
    public function fbRegister(FbSignupRequest $request) {
        $resonse = FbServiceProvider::registerUser($request->all());
        return $this->sendJsonResponse($resonse);
    }

    /**
     * Changes password
     * @param ChangePasswordRequest $request
     * @return type
     */
    public function changePassword(ChangePasswordRequest $request) {
        $response = UserServiceProvider::changePassword($request->all());
        return $this->sendJsonResponse($response);
    }

    public function forgotPassword(ForgotPasswordRequest $request) {
        $response = UserServiceProvider::forgotPassword($request->all());
        return $this->sendJsonResponse($response);
    }

    public function resetPassword(ResetPasswordRequest $request) {
        $response = UserServiceProvider::resetPassword($request->all());
        return $this->sendJsonResponse($response);
    }

    /**
     * This method is used to upload/update profile image of user
     * @param UpdateProfileImageRequest $request
     * @return type
     */
    public function updateProfileImage(UpdateProfileImageRequest $request) {
        $response = UserServiceProvider::updateProfileImage($request->all()['profileImage']);
        return $this->sendJsonResponse($response);
    }
    
    /**
     * This method is used to get user's profile details
     * @return type
     */
    public function getProfileDetail(UserProfileRequest $request) {
        $response = UserServiceProvider::userProfileDetail();
        return $this->sendJsonResponse($response);
    }
    
    public function editPrivacy(Request $request) {
        $response = UserServiceProvider::editPrivacy($request->all());
        return $this->sendJsonResponse($response);
    }

}
