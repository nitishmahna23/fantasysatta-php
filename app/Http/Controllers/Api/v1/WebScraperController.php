<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\BaseController;
use App\Providers\WebScraperProvider;

class WebScraperController extends BaseController {
    
    /**
     * This API fetch all cities of India
     * @return JSON
     */
    public function scrapeCity() {
        $resonse = WebScraperProvider::scrapeCity();
        return $this->sendJsonResponse($resonse);
    }
    
    /**
     * This API fetch price of oil
     * @param integer $oilType
     * @return JSON
     */
    public function scrapeOilPrice($oilType) {
        $resonse = WebScraperProvider::scrapeOilPrice($oilType);
        return $this->sendJsonResponse($resonse);
    }
    
    /**
     * Truncate 1 week old data of oil prices from DB
     * @return JSON
     */
    public function truncateOilPrice() {
        $resonse = WebScraperProvider::truncateOilPrice();
        return $this->sendJsonResponse($resonse);
    }

}
