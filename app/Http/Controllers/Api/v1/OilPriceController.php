<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\BaseController;
use App\Providers\OilPriceProvider;
use App\Http\Requests\OilPriceRequest;

class OilPriceController extends BaseController {
    
    /**
     * This API fetch oil price
     * @return JSON
     */
    public function fetchOilPrice(OilPriceRequest $request) {
        $resonse = OilPriceProvider::fetchOilPrice($request->all());
        return $this->sendJsonResponse($resonse);
    }
    
    /**
     * This API fetch all cities
     * @return JSON
     */
    public function fetchCities() {
        $resonse = OilPriceProvider::fetchCities();
        return $this->sendJsonResponse($resonse);
    }
    
}
