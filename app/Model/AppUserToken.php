<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppUserToken extends Model {

    use SoftDeletes;

    protected $table = 'app_user_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    
    public static function addUserToken($userId){
    
        $userToken = AppUserToken::firstOrNew(['user_id' => $userId]);
        $userToken->user_id = $userId;
        $userToken->access_token = md5(uniqid(mt_rand(), true));
        if($userToken->save()){
            return $userToken->access_token;
        }
        return FALSE;
    }
    
    
}
