<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VerificationToken extends Model {

    protected $table = 'verification_token';
    protected $hidden = ['deleted_at'];
    
     protected $fillable = ['user_id'];

    const EMAIL_VERIFICATION = 1;
    const FORGET_PASSWORD = 2;
    const PASSWORD_EXPIRY_TIME = 24; //In hours
    const EMAIL_EXPIRY_TIME = 24; //In hours
    
    
    
    
    
}
