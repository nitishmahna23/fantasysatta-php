<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ppOilPrice extends Model {

    protected $table = 'pp_oil_price';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Insert prices of all oil types in DB
     * @param Array $priceArr
     * @return Boolean
     */
    public static function updateOilPrice($priceArr) {
        return ppOilPrice::insert($priceArr);
    }
    
    /**
     * Truncate oil price table
     * @return BOOLEAN
     */
    public static function truncateOilPrice() {
        $lastWeekDay = date('Y-m-d h:i:s', strtotime('-1 days'));
        return ppOilPrice::where('created_at','<',$lastWeekDay)->delete();
    }
    
    /**
     * Fetch oil price
     * @param integer $cityId
     * @param integer $oilType
     * @return Array
     */
    public static function fetchOilPrice($cityId,$oilType) {
        return ppOilPrice::where('city_id',$cityId)->where('oil_type',$oilType)->get()->toArray();
    }
    
}
