<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model {

    use SoftDeletes;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    
    const ROLE_TYPE_ADMIN = 1;
    const ROLE_TYPE_APP_USER = 2;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    public static function getUser($email,$role_type){
    
        return User::select('*')->where('email',$email)->where('role_type',$role_type)->first();
    }
    
    public static function fetchReferralCode() {
        return User::pluck('referral_code')->toArray();
    }
    
    
}
