<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ppCity extends Model {

    use SoftDeletes;

    protected $table = 'pp_city';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Insert cities and their ids in DB
     * @param Array $cityArr
     * @return Boolean
     */
    public static function updateCity($cityArr) {
        ppCity::truncate();
        return ppCity::insert($cityArr);
    }
    
    /**
     * Fetch city ids
     * @return array
     */
    public static function fetchCityId() {
        return ppCity::pluck('city_id')->toArray();
    }
    
    /**
     * Fetch all cities data
     * @return array
     */
    public static function fetchCities() {
        return ppCity::get()->toArray();
    }
}
