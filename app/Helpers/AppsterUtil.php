<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AppsterUtil {

    /**
     * This method is used to validate facebook access token
     * @param type $param
     */
    public static function isFacebookTokenValid($facebookId, $facebookAccessToken) {
        $fb_url = "https://graph.facebook.com/me?access_token=" . $facebookAccessToken;
        $json = static::httpCurlGet($fb_url);
        $response = json_decode($json);
        if (empty($response->id) || ($response->id != $facebookId)) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * 
     * @return type
     */
    public static function generateTempPassword() {
        return (substr(md5(microtime()), rand(0, 26), 8));
    }

    /**
     * Method to get time difference in minutes between two dates
     * @return integer
     */
    public static function timeDifference($toTime, $fromTime) {
        return round(abs($toTime - $fromTime) / 60, 2);
    }
    
    /**
     * Generate unique referral code for a user
     * @param string $username
     * @param Array $referralCodeArr
     * @return string
     */
    public static function generateReferralCode($username, $referralCodeArr) {
        $randomString = substr(md5(microtime()), rand(0, 26), 5);
        $initial = substr($username, 0, 2);
        $referralCode = "FS" . $initial . $randomString;
        if (in_array($referralCode, $referralCodeArr)) {
            static::generateReferralCode($username, $referralCodeArr);
        } else {
            return $referralCode;
        }
    }

}
