<?php

namespace App\Helpers;

class Constants {

    const SCRAPE_CITY_URL = 'http://www.mypetrolprice.com/34/Petrol-price-in-Delhi';
    const PETROL_OIL_TYPE = 1;
    const DIESEL_OIL_TYPE = 2;
    const CNG_OIL_TYPE = 3;
    const AUTOGAS_OIL_TYPE = 4;
    const LPG_OIL_TYPE = 5;
    const PETROL_OIL_NAME = 'Petrol';
    const DIESEL_OIL_NAME = 'Diesel';
    const CNG_OIL_NAME = 'CNG';
    const AUTOGAS_OIL_NAME = 'AutoGas';
    const LPG_OIL_NAME = 'LPG';
    const SCRAPE_OIL_URL = 'http://www.mypetrolprice.com/';

}
