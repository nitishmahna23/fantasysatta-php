<?php

namespace App\Providers;

use Mail;
use App\Model\User;
use App\Model\AppUserToken;
use App\Model\VerificationToken;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Helpers\AppsterUtil;

/**
 * UserServiceProvider class contains methods for user management
 * @author Shyam<shyam.pandey@appster.in>
 */
class UserServiceProvider extends BaseServiceProvider {

    /**
     * Logs in user and returns user object
     * @param type $data
     * @return type
     */
    public static function loginUser($data) {
        try {   
            $user = User::getUser($data['email'],$data['roleType']);
            if(!$user) {
                return static::responseError(trans('messages.invalid_login_credentials'), Response::HTTP_UNAUTHORIZED);
            }
            if(\Hash::check($data['password'],$user->password) && $user->status == User::STATUS_ACTIVE) {
                $appUserToken = AppUserToken::addUserToken($user->id);
                $result['user'] = $user;
                $result['accessToken'] = $appUserToken;
                return static::responseSuccess($result, trans('messages.login_success'));
            }
            return static::responseError(trans('messages.invalid_login_credentials'), Response::HTTP_UNAUTHORIZED);
            
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    /**
     * Registers user and return user object
     * @param type $data
     * @return type
     */
    public static function registerUser($data) {
        try {
            $userObj = new User();
            $userObj->email = $data['email'];
            $userObj->password = \Hash::make($data['password']);
            $userObj->name = $data['name'];
            $userObj->username = $data['username'];
            $userObj->mobile_number = $data['mobileNumber'];
            $userObj->role_type = $data['roleType'];
            $userObj->status = User::STATUS_INACTIVE;
            $referralCodeArr = User::fetchReferralCode();
            $referralCode = AppsterUtil::generateReferralCode($userObj->username,$referralCodeArr);
            $userObj->referral_code = $referralCode;
            if ($userObj->save()) {
                $token = md5(uniqid(mt_rand(), true));
                $expiryDate = date("Y-m-d H:i:s", strtotime('+' . VerificationToken::EMAIL_EXPIRY_TIME . ' hours'));
                $verificationToken = new VerificationToken();
                $verificationToken->user_id = $userObj->id;
                $verificationToken->type = VerificationToken::EMAIL_VERIFICATION;
                $verificationToken->token = $token;
                $verificationToken->expiry_date = $expiryDate;
                $verificationToken->save();
                $link = url('verify-email') . '?token=' . $token;
                $email = $userObj->email;
                \Mail::send('emails.email_verification', ['name' => ucwords($userObj->name), 'link' => $link], function($message) use ($email) {
                    $message->to($email)->subject(trans('messages.email_verification_subject'));
                    $message->from(env('MAIL_FROM'), trans('messages.org_name'));
                });
                return static::responseSuccess('',trans('messages.register_success'));
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    /**
     * Updates user profile
     * @param type $data
     * @return type
     */
    public static function updateProfile($data) {
        try {
            $userObj = \Auth::user();
            
            // Update name only 
            if(isset($data['isNameOnly'])) {
                $userObj->name = $data['name'];
                $userObj->save();
                $userModel = new User();
                $result['user'] = $userModel->getUser($userObj->id);
                return static::responseSuccess($result, trans('messages.profile_created'));
            }
            
            $userStatus = $userObj->status;
            if (!empty($data['email'])) {
                $userObj->email = $data['email'];
            }
            $temp_pass = null;
            if (empty($userObj->password)) {
                $temp_pass = AppsterUtil::generateTempPassword();
                $userObj->password = \Hash::make($temp_pass);
            }

            $userObj->username = $data['username'];
            $userObj->name = $data['name'];
            $userObj->status = User::STATUS_APPROVED;
            
            
            DB::beginTransaction();
            if ($userObj->save()) {
                // Add referral code if used
                if (!empty($data['referralCode'])) {
                    $referrer = User::where('referral_code', $data['referralCode'])->first();
                    $userObj->referred_user_id = $referrer->id;
                    $userObj->save();
                }

                if ($userStatus == User::STATUS_INCOMPLETE_PROFILE) {
                    $token = md5(uniqid(mt_rand(), true));
                    $verificationToken = new VerificationToken();
                    $verificationToken->user_id = $userObj->id;
                    $verificationToken->type = VerificationToken::EMAIL_VERIFICATION;
                    $verificationToken->token = $token;
                    $verificationToken->save();
                    $link = url('verify-email') . '?token=' . $token;
                    Mail::queue('emails.email_verification', ['name' => ucwords($userObj->name), 'password' => $temp_pass, 'link' => $link], function($message) use ($userObj) {
                        $message->to($userObj->email, ucwords($userObj->name))->subject(trans('messages.email_verification_subject'));
                        $message->from(env('MAIL_FROM'), trans('messages.org_name'));
                    });
                }
                DB::commit();
                $userModel = new User();
                $result['user'] = $userModel->getUser($userObj->id);
                return static::responseSuccess($result, trans('messages.profile_created'));
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    public static function forgotPassword($data) {
        try {
            $user = User::where('email', '=', $data['email'])->where('role_type', '=', $data['roleType'])->first();
            $token = md5(uniqid(mt_rand(), true));
            $expiryDate = date("Y-m-d H:i:s", strtotime('+' . VerificationToken::PASSWORD_EXPIRY_TIME . ' hours'));
            $verificationToken = new VerificationToken();
            $verificationToken->user_id = $user->id;
            $verificationToken->type = VerificationToken::FORGET_PASSWORD;
            $verificationToken->token = $token;
            $verificationToken->expiry_date = $expiryDate;
            $verificationToken->save();
            $link = url('reset-password') . '?token=' . $token;
            Mail::queue('emails.forgot-password', ['name' => ucwords($user->name), 'email' => $user->email, 'org_name' => trans('messages.org_name'), 'link' => $link, 'hours' => VerificationToken::PASSWORD_EXPIRY_TIME], function($message) use ($user) {
                $message->to($user->email, ucwords($user->name))->subject('Reset Password Link');
                $message->from(env('MAIL_FROM'), trans('messages.org_name'));
            });

            return static::responseSuccess(null, trans('messages.forgot_pwd_email'));
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    public static function getVerificationTokenDetails($data) {
        try {
            $current_date = date('Y-m-d H:i:s');
            $result['token_details'] = VerificationToken::where('token', '=', $data['token'])->where('expiry_date', '>=', $current_date)->first();
            if ($result['token_details']) {
                return static::responseSuccess($result);
            } else {
                return static::responseError(trans('messages.verification_token_expired'), Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    public static function logout($access_token) {
        try {
            AppUserToken::where('access_token', '=', $access_token)->delete();
            \Auth::logout();
            return static::responseSuccess(null, trans('messages.logout'));
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    public static function verifyEmail($data) {
        try {
            $verification_token = VerificationToken::where('token', '=', $data['token'])->where('type', '=', VerificationToken::EMAIL_VERIFICATION)->first();
            if (count($verification_token) > 0) {
                $user = User::findOrFail($verification_token->user_id);
                $user->is_email_verified = 1;
                if ($user->save()) {
                    $verification_token->delete();
                    return static::responseSuccess();
                }
            } else {
                return static::responseError();
            }
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    /**
     * function is used to change user password
     */
    public static function changePassword($data) {
        try {
            $user = \Auth::user();
            $isOldPasswordValid = \Hash::check($data['oldPassword'], $user->password);
            if ($isOldPasswordValid) {
                $hashed_password = \Hash::make($data['newPassword']);
                User::where('id', '=', $user->id)->update(['password' => $hashed_password]);
                return static::responseSuccess(null, trans('messages.password_changed'));
            } else {
                return static::responseError(trans('messages.invalid_old_password'), Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    public static function resetPassword($data) {
        try {
            $hashed_password = \Hash::make($data['password']);
            User::where('id', '=', $data['userId'])->update(['password' => $hashed_password]);
            VerificationToken::where('user_id', '=', $data['userId'])->where('type', '=', VerificationToken::FORGET_PASSWORD)->delete();
            DB::commit();
            return static::responseSuccess(null, trans('messages.password_changed'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    public static function updateProfileImage($imageData) {
        try {
            if (!empty($imageData)) {
                $user = \Auth::user();
                if (is_file($imageData)) {
                    $extension = !empty($imageData->getClientOriginalExtension()) ? $imageData->getClientOriginalExtension() : 'jpeg';
                    $path = $imageData->getPathName();
                    $filename = AppsterUtil::uploadImageToStorage($path, 'users/profile/' . uniqid() . $user->id . '.' . $extension);
                }

                if ($filename) {
                    if (!empty($user->profile_image)) {
                        AppsterUtil::deleteImageFromStorage($user->profile_image);
                    }
                    $user->profile_image = $filename;
                    $user->save();
                    $response['image'] = env('S3_URL') . $filename;
                    return static::responseSuccess($response, trans('messages.image_upload_success'));
                }
                return static::responseError(trans('messages.image_upload_error'));
            }
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    public static function userProfileDetail() {
        try {
            $currentUserId = \Auth::user()->id;
            $userObj = new User();
            $userDetail = $userObj->getUser($currentUserId);            
            $response['user']=$userDetail;
            $response['transactionHistory']= array();
            return static::responseSuccess($response);
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }
    
    public static function editPrivacy($data) {
        $isPublic = $data['is_public'];
        $currentUserId = \Auth::user()->id;
        $fetchFollowers = User::fetchFollowers($currentUserId);
    }

}
