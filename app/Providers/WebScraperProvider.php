<?php

namespace App\Providers;

use App\Model\ppCity;
use App\Model\ppOilPrice;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Helpers\Constants;

/**
 * StockSymbolProvider class contains methods for stock symbol management
 */
class WebScraperProvider extends BaseServiceProvider {

    /**
     * Method to fetch all cities of India
     * @return boolean
     */
    public static function scrapeCity() {
        try {
            $goutteClient = new Client();
            $guzzleClient = new GuzzleClient(array(
                'timeout' => 60,
            ));
            $goutteClient->setClient($guzzleClient);
            $crawler = $goutteClient->request('GET', Constants::SCRAPE_CITY_URL);
            $cityArr = $crawler->filter('select#CPH1_ctl01_1_DropDownList1_1 option')->each(function ($node) {
                $city['city_id'] = $node->attr('value');
                $city['city_name'] = $node->text();
                return $city;
            });
            DB::beginTransaction();
            ppCity::updateCity($cityArr);
            DB::commit();
            return static::responseSuccess('', trans('messages.cities_updated'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

    /**
     * API to fetch prices of oil
     * @param integer $oilType
     * @return JSON
     */
    public static function scrapeOilPrice($oilType) {
        try {
            $goutteClient = new Client();
            $guzzleClient = new GuzzleClient(array(
                'timeout' => 60,
            ));
            $fetchCityId = ppCity::fetchCityId();
            if($oilType == Constants::PETROL_OIL_TYPE) {
                $oilName = Constants::PETROL_OIL_NAME;
            }
            if($oilType == Constants::DIESEL_OIL_TYPE) {
                $oilName = Constants::DIESEL_OIL_NAME;
            }
            if($oilType == Constants::CNG_OIL_TYPE) {
                $oilName = Constants::CNG_OIL_NAME;
            }
            if($oilType == Constants::AUTOGAS_OIL_TYPE) {
                $oilName = Constants::AUTOGAS_OIL_NAME;
            }
            if($oilType == Constants::LPG_OIL_TYPE) {
                $oilName = Constants::LPG_OIL_NAME;
            }
            for ($i = 1; $i < count($fetchCityId); $i++) {
                $goutteClient->setClient($guzzleClient);
                $crawler = $goutteClient->request('GET', Constants::SCRAPE_OIL_URL.$fetchCityId[$i].'/'.$oilName.'-price-in-Delhi');
                $GLOBALS['cityId'] = $fetchCityId[$i];
                $GLOBALS['oilType'] = $oilType;
                $crawlerResult = $crawler->filter('#CPH1_lblCurrent')->each(function ($node) {
                    $oilPrice['city_id'] = $GLOBALS['cityId'];
                    $oilPrice['oil_type'] = $GLOBALS['oilType'];
                    $priceArr = explode(' = ',$node->text());
                    if(isset($priceArr[1])) {
                        $finalPriceArr = explode(' Rs/',$priceArr[1]);
                        $oilPrice['price'] = $finalPriceArr[0];
                    } else {
                        $oilPrice['price'] = 0;
                    }
                    return $oilPrice;
                });
                $oilPriceArr[] = $crawlerResult[0];
            }
            DB::beginTransaction();
            ppOilPrice::updateOilPrice($oilPriceArr);
            DB::commit();
            return static::responseSuccess('', trans('messages.price_updated'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }
    
    public static function truncateOilPrice() {
        try {
            DB::beginTransaction();
            ppOilPrice::truncateOilPrice();
            DB::commit();
            return static::responseSuccess('', trans('messages.price_table_truncated'));
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

}
