<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Response;

/**
 * BaseServiceProvider works as a base class for all user defined providers
 */
class BaseServiceProvider extends ServiceProvider {

    /**
     * The default response format returned by a service
     *
     * @var array
     * message key contains success/error message
     * success key contains true/false
     * errors key contains array of key-value validation errors for each input field in json, if validation fails
     * status_code key contains HTTP STATUS CODE based on response
     */
    protected static $data = [
        'message' => '',
        'success' => false,
        'errors' => [],
        'status_code' => Response::HTTP_OK
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    public static function responseSuccess($data = array(), $message = '', $statusCode = Response::HTTP_OK) {

        $response = array(
            'success' => true,
            'status_code' => $statusCode,
            'message' => $message
        );

        if (!empty($data)) {
            while ($res = current($data)) {
                $key = key($data);
                if (!empty($key)) {
                    $response[$key] = $res;
                }
                next($data);
            }
        }
        return $response;
    }

    public static function responseError($message = '', $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR, $data = array()) {
        $key = !empty($data) ? key($data) : '';
       
        $response = array(
            'success' => false,
            'status_code' => $statusCode,
            'message' => $message
        );
        if (!empty($key)) {
            $response[$key] = (object) $data[$key];
        }
        
        return $response;
    }
    
}
