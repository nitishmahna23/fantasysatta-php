<?php

namespace App\Providers;

use App\Model\ppCity;
use App\Model\ppOilPrice;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Helpers\Constants;

/**
 * StockSymbolProvider class contains methods for stock symbol management
 */
class OilPriceProvider extends BaseServiceProvider {

    /**
     * Method to fetch oil price
     * @return boolean
     */
    public static function fetchOilPrice($data) {
        try {
            $cityId = $data['city_id'];
            $oilType = $data['oil_type'];
            $oilPrice['data'] = ppOilPrice::fetchOilPrice($cityId,$oilType);
            return static::responseSuccess($oilPrice);
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }
    
    /**
     * Fetch all cities
     * @return JSON
     */
    public static function fetchCities() {
        try {
            $cities['data'] = ppCity::fetchCities();
            return static::responseSuccess($cities);
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return static::responseError(trans('messages.exception_msg'));
        }
    }

}